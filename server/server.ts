import cors from 'cors'
import express, { NextFunction, Request, Response } from 'express'
import { print } from 'listening-on'
import { HTTPError } from './http.error'
import { ShopController } from './shop.controller'
import { ShopService } from './shop.service'

let app = express()

app.use(cors())
app.use(express.static('public'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

let shopService = new ShopService()
let shopController = new ShopController(shopService)
app.use(shopController.router)

app.use((req: Request, res: Response, next: NextFunction) => {
  res.status(404)
  res.json({
    error: 'route not matched',
    url: req.url,
    method: req.method,
  })
})

app.use((error: HTTPError, req: Request, res: Response, next: NextFunction) => {
  res.status(error.statusCode || 500)
  res.json({ error: String(error) })
})

let port = 8100
app.listen(port, () => {
  print(port)
})
