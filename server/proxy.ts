import { proxySchema } from 'better-sqlite3-proxy'
import { db } from './db'

export type Shop = {
  id?: number | null
  name: string
  address: string
  delete_time: string | null
}

export type Feature = {
  id?: number | null
  name: string
}

export type ShopFeature = {
  id?: number | null
  shop_id: number
  shop?: Shop
  feature_id: number
  feature?: Feature
}

export type User = {
  id?: number | null
}

export type ShopAdmin = {
  id?: number | null
  shop_id: number
  shop?: Shop
  user_id: number
  user?: User
}

export type DBProxy = {
  shop: Shop[]
  feature: Feature[]
  shop_feature: ShopFeature[]
  user: User[]
  shop_admin: ShopAdmin[]
}

export let proxy = proxySchema<DBProxy>({
  db,
  tableFields: {
    shop: [],
    feature: [],
    shop_feature: [
      /* foreign references */
      ['shop', { field: 'shop_id', table: 'shop' }],
      ['feature', { field: 'feature_id', table: 'feature' }],
    ],
    user: [],
    shop_admin: [
      /* foreign references */
      ['shop', { field: 'shop_id', table: 'shop' }],
      ['user', { field: 'user_id', table: 'user' }],
    ],
  },
})
