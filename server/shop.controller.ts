import { array, id, object, string } from 'cast.ts'
import { Request, Router } from 'express'
import { wrapMethod } from './http.controller'
import { ShopService } from './shop.service'

export class ShopController {
  router = Router()

  constructor(public shopService: ShopService) {
    this.router.get('/shops/:id/detail', wrapMethod(this.getShopDetail))
    this.router.patch('/shops/:id', wrapMethod(this.updateShop))
    this.router.delete('/shops/:id', wrapMethod(this.deleteShop))
    this.router.get('/shops', wrapMethod(this.getShopList))
    // this.router.post('/shop', wrapMethod(this.getShopDetail))
  }

  getShopList = (req: Request) => {
    return this.shopService.getShopList()
  }

  getShopDetail = (req: Request) => {
    let parser = object({
      params: object({ id: id() }),
    })
    let shop_id = parser.parse(req).params.id
    return this.shopService.getShopDetail(shop_id)
  }

  updateShop = (req: Request) => {
    let parser = object({
      params: object({ id: id() }),
      body: object({
        shop: object({
          name: string(),
          address: string(),
        }),
        feature_ids: array(id()),
      }),
    })
    let { params, body } = parser.parse(req)
    return this.shopService.updateShop(params.id, body.shop, body.feature_ids)
  }

  deleteShop = (req: Request) => {
    let parser = object({
      params: object({ id: id() }),
    })
    let { params } = parser.parse(req)
    return this.shopService.deleteShop(params.id)
  }
}
