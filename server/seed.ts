import { proxy } from './proxy'

proxy.user[1] = {}

proxy.shop[1] = { name: 'shop 1', address: 'street 1', delete_time: null }

proxy.shop_admin[1] = {
  shop_id: 1,
  user_id: 1,
}

proxy.feature[1] = { name: 'fea 1' }
proxy.feature[2] = { name: 'fea 2' }
proxy.feature[3] = { name: 'fea 3' }

proxy.shop_feature[1] = {
  shop_id: 1,
  feature_id: 1,
}
proxy.shop_feature[2] = {
  shop_id: 1,
  feature_id: 3,
}
