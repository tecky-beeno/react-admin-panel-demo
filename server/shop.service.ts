import { proxy, Shop } from './proxy'
import { filter, toSqliteTimestamp, unProxy } from 'better-sqlite3-proxy'
import { HTTPError } from './http.error'

export class ShopService {
  constructor() {}

  getShopList() {
    // this.knex('shop').where({delete_time: null}).select('id', 'name')

    let shops = filter(proxy.shop, { delete_time: null }).map(shop => ({
      id: shop.id,
      name: shop.name,
    }))

    return {
      shops,
    }
  }

  getShopDetail(shop_id: number) {
    // this.knex('shop_feature').where({ shop_id }).select('feature_id')

    let shop = proxy.shop[shop_id]

    if (!shop) throw new HTTPError(404, 'shop not found')

    return {
      shop: {
        id: shop_id,
        name: shop.name,
        address: shop.address,
      },
      has_features: filter(proxy.shop_feature, { shop_id }).map(
        feature => feature.feature_id,
      ),
      all_features: unProxy(proxy.feature),
    }
  }

  updateShop(shop_id: number, shop: Partial<Shop>, feature_ids: number[]) {
    // this.knex('shop').where({ id: shop_id }).update(shop)
    Object.assign(proxy.shop[shop_id], shop)

    // this.knex('shop_feature').where({shop_id}).delete()
    filter(proxy.shop_feature, { shop_id }).forEach(
      row => delete proxy.shop_feature[row.id!],
    )

    for (let feature_id of feature_ids) {
      // this.knex('shop_feature').insert({shop_id, feature_id})
      proxy.shop_feature.push({ shop_id, feature_id })
    }

    return {}
  }

  deleteShop(shop_id: number) {
    // this.knex('shop').where({ id: shop_id }).delete()
    // delete proxy.shop[shop_id]

    // this.knex('shop').where({ id: shop_id }).update({delete_time: this.knex.fn.now()})
    proxy.shop[shop_id].delete_time = toSqliteTimestamp(new Date())

    return {}
  }
}

// let shopService = new ShopService()
// console.log(shopService.getShopDetail(1))
