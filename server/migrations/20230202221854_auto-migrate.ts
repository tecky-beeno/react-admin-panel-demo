import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  await knex.raw('alter table `shop_feature` drop column `feature_fk`')
  await knex.raw('alter table `shop_feature` add column `feature_id` integer not null references `feature`(`id`)')
}


export async function down(knex: Knex): Promise<void> {
  await knex.raw('alter table `shop_feature` drop column `feature_id`')
  await knex.raw('alter table `shop_feature` add column `feature_fk` integer not null')
}
