import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  await knex.raw('alter table `shop` add column `delete_time` timestamp null')
}


export async function down(knex: Knex): Promise<void> {
  await knex.raw('alter table `shop` drop column `delete_time`')
}
