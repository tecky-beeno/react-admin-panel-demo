import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {

  if (!(await knex.schema.hasTable('shop'))) {
    await knex.schema.createTable('shop', table => {
      table.increments('id')
      table.text('name').notNullable()
      table.text('address').notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('feature'))) {
    await knex.schema.createTable('feature', table => {
      table.increments('id')
      table.text('name').notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('shop_feature'))) {
    await knex.schema.createTable('shop_feature', table => {
      table.increments('id')
      table.integer('shop_id').unsigned().notNullable().references('shop.id')
      table.integer('feature_fk').notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('user'))) {
    await knex.schema.createTable('user', table => {
      table.increments('id')
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('shop_admin'))) {
    await knex.schema.createTable('shop_admin', table => {
      table.increments('id')
      table.integer('shop_id').unsigned().notNullable().references('shop.id')
      table.integer('user_id').unsigned().notNullable().references('user.id')
      table.timestamps(false, true)
    })
  }
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('shop_admin')
  await knex.schema.dropTableIfExists('user')
  await knex.schema.dropTableIfExists('shop_feature')
  await knex.schema.dropTableIfExists('feature')
  await knex.schema.dropTableIfExists('shop')
}
