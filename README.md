# CRUD Admin Panel (Express MVC + React)

Demo of rapid development using [quick-erd](https://www.npmjs.com/package/quick-erd), [better-sqlite3-proxy](https://www.npmjs.com/package/better-sqlite3-proxy), [cast.ts](https://www.npmjs.com/package/cast.ts) and [ionic](https://ionicframework.com/)

Youtube Playlist: https://www.youtube.com/playlist?list=PLjB7odcPYBI31J3fMgY8xi3AWcHdnoi_X

Git Repo: https://gitlab.com/tecky-beeno/react-admin-panel-demo
